<?php
//laravel 服务容器

//注册服务提供者时绑定到服务容器 $this->app->bind
$this->app->bind('HelpSpot\API', function ($app) {
    return new HelpSpot\API($app['HttpClient']);
});

//只使用一次
$this->app->singleton('FooBar', function ($app) {
    return new FooBar($app['SomethingElse']);
});

//绑定实例
$fooBar = new FooBar(new SomethingElse);
$this->app->instance('FooBar', $fooBar);

//绑定实现的接口
$this->app->bind('App\Contracts\EventPusher', 'App\Services\RedisEventPusher');
use App\Contracts\EventPusher;
public function __construct(EventPusher $pusher)
{
    $this->pusher = $pusher;
}

//上下级绑定
$this->app->when('App\Handlers\Commands\CreateOrderHandler')
                ->needs('App\Contracts\EventPusher')
                ->give('App\Services\PubNubEventPusher');
$this->app->when('App\Handlers\Commands\CreateOrderHandler')
                ->needs('App\Contracts\EventPusher')
                ->give(function () {
                // Resolve dependency...
                });
//绑定原形
$this->app->when('App\Handlers\Commands\CreateOrderHandler')
          ->needs('$maxOrderCount')
          ->give(10);

//绑定标记
$this->app->bind('SpeedReport', function () {
    //
});

$this->app->bind('MemoryReport', function () {
    //
});
$this->app->tag(['SpeedReport', 'MemoryReport'], 'reports');
$this->app->bind('ReportAggregator', function ($app) {
    return new ReportAggregator($app->tagged('reports'));
});

//使用服务 包含控制器 事件监听 队列工作 中间件 更多
$fooBar = $this->app->make('FooBar');
$fooBar = $this->app['FooBar'];

//服务容器事件
$this->app->resolving(function ($object, $app) {
    // Called when container resolves object of any type...
});
$this->app->resolving(FooBar::class, function (FooBar $fooBar, $app) {
    // Called when container resolves objects of type "FooBar"...
});
