<?php
//laravel 邮件

//驱动器必装
composer.json
"guzzlehttp/guzzle": "~5.3|~6.0"
config/mail.php
'driver' => 'mailgun'
config/services.php
'mailgun' => [
    'domain' => 'your-mailgun-domain',
    'secret' => 'your-mailgun-key',
],

//Mandrill驱动
config/mail.php
'driver' => 'mandrill'
config/services.php
'mandrill' => [
    'secret' => 'your-mandrill-key',
],

//SparkPost驱动
config/mail.php
'driver' => 'sparkpost'
config/services.php
'sparkpost' => [
    'secret' => 'your-sparkpost-key',
],

//SES驱动
composer.json
"aws/aws-sdk-php": "~3.0"
config/mail.php
'driver' => 'ses'
config/services.php
'ses' => [
    'key' => 'your-ses-key',
    'secret' => 'your-ses-secret',
    'region' => 'ses-region',  // e.g. us-east-1
],

//使用
public function sendEmailReminder(Request $request, $id)
{
    $user = User::findOrFail($id);
    Mail::send('emails.reminder', ['user' => $user], function ($message) use ($user) {
        $message->from('hello@app.com', 'Your Application');
        $message->to($user->email, $user->name)->subject('Your Reminder!');
    });
}

$message->from($address, $name = null);
$message->sender($address, $name = null);
$message->to($address, $name = null);
$message->cc($address, $name = null);
$message->bcc($address, $name = null);
$message->replyTo($address, $name = null);
$message->subject($subject);
$message->priority($level);
$message->attach($pathToFile, array $options = []);
$message->attach($pathToFile, ['as' => $display, 'mime' => $mime]);
// Attach a file from a raw $data string...
$message->attachData($data, $name, array $options = []);
$message->attachData($pdf, 'invoice.pdf', ['mime' => $mime]);
// Get the underlying SwiftMailer message instance...
$message->getSwiftMessage();

Mail::send(['html.view', 'text.view'], $data, $callback);
//text e-mail
Mail::send(['text' => 'view'], $data, $callback);

Mail::raw('Text to e-mail', function ($message) {
    //
});

//发送的模板包含图片 数据
<body>
    Here is an image:

    <img src="<?php echo $message->embed($pathToFile); ?>">
</body>
<body>
    Here is an image from raw data:

    <img src="<?php echo $message->embedData($data, $name); ?>">
</body>

//邮件队列
Mail::queue('emails.welcome', $data, function ($message) {
    //
});

//延迟发送
Mail::later(5, 'emails.welcome', $data, function ($message) {
    //
});

//按队列执行
Mail::queueOn('queue-name', 'emails.welcome', $data, function ($message) {
    //
});
Mail::laterOn('queue-name', 5, 'emails.welcome', $data, function ($message) {
    //
});

//本地日志使用邮件
config/mail.php
'to' => [
    'address' => 'dev@domain.com',
    'name' => 'Dev Example'
],

//事件 非队列
protected $listen = [
    'Illuminate\Mail\Events\MessageSending' => [
        'App\Listeners\LogSentMessage',
    ],
];